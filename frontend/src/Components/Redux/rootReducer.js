import {combineReducers} from 'redux';
import UserReducer from './Reducer';
export default combineReducers(
    {
      users : UserReducer
    }
)
