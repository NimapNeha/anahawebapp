const initialState ={
    users:{},
    error_msg:null,
    success_msg:null,
    loading:true,
    isSuccess:false,
}

const  UserReducer =(state=initialState, action)=>{
    switch(action.type){
        case "ON_GETUSER_SUCCESS":{
            return {
                ...state,
                users:action.payload,
                loading:false,

            }
        }
        default:{
            return {
                ...state
            }
        }
    }
}

    
export default UserReducer 