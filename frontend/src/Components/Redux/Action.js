import { BASE_URL } from "../resources/APIEndpoints"
import { APIServices } from "../resources/APIServices"


export const onGetUsers = () =>{
    return async(dispatch)=>{
        const url = BASE_URL
        const res = await new APIServices().get(url)//use service
        .then((res)=>{
            //console.log(res.results)
            dispatch(onGetUserSuccess(res.results))
        })
        .catch((error)=>{
            console.log(error)
            //cogo toast message
        })
    }
}

export const onGetUserSuccess = (data) =>{
    return {
        type : "ON_GETUSER_SUCCESS",
        payload : data,
    }
}