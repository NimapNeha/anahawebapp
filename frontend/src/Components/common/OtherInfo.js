import React from 'react'
import ErrorIcon from '@material-ui/icons/Error';
import AppsIcon from '@material-ui/icons/Apps';
import NotificationsIcon from '@material-ui/icons/Notifications';
function OtherInfo() {
    return (
        <div>

                <ErrorIcon style={{fontSize:32}}/>
                <AppsIcon style={{fontSize:32, marginLeft:30}}/>
                <NotificationsIcon style={{fontSize:32, marginLeft:30}}/>  
    
        </div>
    )
}

export default OtherInfo
