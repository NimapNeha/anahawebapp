import React from 'react'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
function ProfileInfo() {
    return (
        <div>
            <div style={{marginTop:-35}}>
            <AccountCircleIcon style={{fontSize:45, marginLeft:1300}}/>
            </div>
            <div style={{marginTop:-40, marginLeft:1355, fontSize:18 }}>
            <span>
                Dr.Raquel
            </span>
            <div style={{marginTop:-28, marginLeft:78}}>
                <KeyboardArrowDownIcon style={{fontSize:35}}/>
            </div>
            </div>
        </div>
    )
}

export default ProfileInfo
