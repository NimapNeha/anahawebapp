import SearchIcon from '@material-ui/icons/Search'
import { Box, fade, InputBase, makeStyles } from '@material-ui/core'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import React from 'react'

function Search() {
    return (
        <div style={{marginRight:550}}>
            <Box bgcolor='white' style={{marginLeft:520,border:"1px solid rgba(0, 0, 0, 0.274)", height:50, width:450}}>
                    
                <SearchIcon style={{marginTop:8, marginLeft:10, fontSize:35, fontWeight:'normal'}} />
                <div style={{marginTop:-40}}>
                    <InputBase  style={{marginLeft:50, fontSize:18, color:'black'}} placeholder="Search">
                    </InputBase>
                    
                </div>
                <div style={{marginTop:-33}}>
                    <KeyboardArrowDownIcon style={{ marginLeft:400, fontSize:35}}/>
                </div>
            </Box>
            
        </div>
    )
}

export default Search
