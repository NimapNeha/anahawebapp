import React, { Fragment, useEffect } from 'react'
import Search from '../common/Search';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ProfileInfo from '../common/ProfileInfo';
import OtherInfo from '../common/OtherInfo';
import { Box, CircularProgress, makeStyles, Paper, Tab, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, Toolbar } from '@material-ui/core';
import { onGetUsers } from '../Redux/Action';
import { useDispatch, useSelector } from 'react-redux'; 
const useStyles = makeStyles({

})

const columns = [
    //{id: 'category', label: 'Category', minWidth: 100, fontWeight:'bold', backgroundColor:'white'},
    {id: 'name', label: 'Name', width: '5%', fontWeight:'bold', backgroundColor:'white', color: '#2d3667'},
    {id: 'usernme', label: 'Username', width: '8%', fontWeight:'bold', backgroundColor:'white', color: '#2d3667'},
    {id: 'phone', label: 'Phone Number', width: '8%', fontWeight:'bold', backgroundColor:'white', color: '#2d3667'},
    {id: 'email', label: 'Email', width: '8%', fontWeight:'bold', backgroundColor:'white',color: '#2d3667'},
    {id: 'city', label: 'City', width: '8%', fontWeight:'bold', backgroundColor:'white',color: '#2d3667'}, 
];
function Compliance() {
    const classes = useStyles()
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(onGetUsers());
    }, [])
    
    const List = useSelector((state) => {
        return state.users;
    })

    
    const {loading, users} = List
    //console.log(users)
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
      };
    const UserAfterPagination = ()=>{
        return users.slice(page*rowsPerPage,(page+1)*rowsPerPage)
    }
    return (

        <Fragment>
            <div style={{marginTop:-210}}>
          <Search/>
        </div> 
        <div style={{marginLeft:260 , marginTop:-40}}> 
            <ArrowForwardIosIcon ></ArrowForwardIosIcon>
            <div style={{marginLeft:25, marginTop:-30, fontSize:18}}>
                <span>
                    Compliance
                </span>
            </div>
        </div>
        <div>
            <ProfileInfo/>
        </div>
        <div style={{marginLeft:1000, marginTop:-35}}>
            <OtherInfo/>
        </div>
            <div style={{marginTop:147, marginLeft:-10}}>
            <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)", width:1465,height:520, borderBottomRightRadius:10, borderBottomLeftRadius:10, borderTopRightRadius:10 }}>
            <p style={{marginLeft:10, marginTop:10}}>Backend Integration</p>
            <Paper elevation={3} style={{height:400, marginTop:-490, width:1440, marginLeft:10, boxShadow:"none"}}>
                <Toolbar>
                    <span style={{fontWeight:'bold'}}>Users Data</span>
                </Toolbar>
                <TableContainer style={{height:340}}>
                    {loading ? <CircularProgress />
                    :
                    <Table>
                         <TableHead>
                            <TableRow>
                            {columns.map((column) => (
                                    <TableCell
                                    key={column.id}
                                    align='justify'
                                    style={{width: column.width, fontWeight:column.fontWeight,color:column.color,
                                        marginLeft:column.marginLeft, backgroundColor:column.backgroundColor }}
                                    >
                                    {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {users && users.length > 0?
                            UserAfterPagination().map((element, index)=>(
                                <TableRow key={index}>
                                    <TableCell>{element.name}</TableCell>
                                    <TableCell>{element.username}</TableCell>
                                    <TableCell>{element.phone}</TableCell>
                                    <TableCell>{element.email}</TableCell>
                                    <TableCell>{element.address.city}</TableCell>
                                </TableRow>
                            ))
                        :<div>
                            <span>No data found</span>
                        </div>
                        }
                        </TableBody>
                    </Table>

                    }
                </TableContainer>
            </Paper>
            <Paper elevation={3} style={{boxShadow:"none"}}> 
            <div style={{width:1440}}>
            {users && users.length > 0? 
                    <TablePagination
                    style={{marginRight:10, width:1440,}}
                    //component="div"
                    rowsPerPageOptions={[5,10,15]}
                    count={users.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    />:""
                }
            </div>
            
            </Paper>
            </Box>
        </div>
        </Fragment>
    )
}

export default Compliance
