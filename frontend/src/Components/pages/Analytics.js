import React from 'react'
import Search from '../common/Search';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ProfileInfo from '../common/ProfileInfo';
import OtherInfo from '../common/OtherInfo';
import { Box, Grid } from '@material-ui/core';
import HealthCondition from './Graphs/HealthCondition';
import HeartRate from './Graphs/HeartRate';
import SleepPattern from './Graphs/SleepPattern';
import Activity from './Graphs/Activity';
import BloodPressure from './Graphs/BloodPressure';
import BodyTemperature from './Graphs/BodyTemperature';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
function Analytics() {
    const Calories = 230
    return (
        <div>
            
        <div style={{marginTop:-210}}>
          <Search/>
        </div> 
        <div style={{marginLeft:260 , marginTop:-40}}> 
            <ArrowForwardIosIcon ></ArrowForwardIosIcon>
            <div style={{marginLeft:25, marginTop:-30, fontSize:18}}>
                <span>
                Analytics
                </span>
            </div>
        </div>
        <div>
            <ProfileInfo/>
        </div>
        <div style={{marginLeft:1000, marginTop:-35}}>
            <OtherInfo/>
        </div>
        <div style={{marginTop:20, marginLeft:-15}}>
            <Box style={{border:"2px solid #F1F1F1", width:1465,height:50, borderRadius:10}}>
                <div style={{marginTop:10}}>
                <span style={{color:'#6787DB', marginLeft:50, fontSize:18}}>Priya Mehta 32Y 4M</span>
                <span style={{ marginLeft:110, fontSize:18}}>ID : P0987</span>
                <span style={{ marginLeft:110, fontSize:18}}>With</span>
                <span style={{color:'#6787DB', marginLeft:5, fontSize:18}}>G</span>
                <span style={{ marginLeft:1, fontSize:18}}>3</span>
                <span style={{color:'#6787DB', marginLeft:5, fontSize:18}}>P</span>
                <span style={{ marginLeft:1, fontSize:18}}>0</span>
                <span style={{color:'#6787DB', marginLeft:5, fontSize:18}}>L</span>
                <span style={{ marginLeft:1, fontSize:18}}>1</span>
                <span style={{color:'#6787DB', marginLeft:5, fontSize:18}}>A</span>
                <span style={{ marginLeft:1, fontSize:18}}>1</span>
                <span style={{ marginLeft:110, fontSize:18}}>8 weeks pregnant</span>
                <span style={{ marginLeft:110, fontSize:18}}>LMP : 20/01/2020</span>
                <span style={{ marginLeft:110, fontSize:18}}>EDD : 29/10/2020</span>
                </div>
            </Box>
        </div>
        <div style={{marginTop:77, marginLeft:-10}}>
            <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)", width:1465,height:530, borderBottomRightRadius:10, borderBottomLeftRadius:10, borderTopRightRadius:10 }}>
                <div style={{marginTop:15, marginLeft:20}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:280, height:130}}>
                        <Activity/>
                    </Box>
                    
                </div>
                <div style={{marginTop:-130, marginLeft:310}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:280, height:130}}>
                        <HeartRate/>
                    </Box>
                    
                </div>
                <div style={{marginTop:-130, marginLeft:600}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:280, height:130}}>
                        <BloodPressure/>
                    </Box>
                    
                </div>
                <div style={{marginTop:-130, marginLeft:890}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:280, height:130}}>
                        <BodyTemperature/>
                    </Box>
                    
                </div>
                <div style={{marginTop:-130, marginLeft:1180}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:250, height:130}}>
                        <p style={{marginTop:5, marginLeft:10,  fontSize:20}}>Past Appointment</p>
                        <p style={{marginLeft:10, marginTop:-80 , fontSize:18, color:'#6787DB'}}>10 AM - 11 AM</p>
                        <p style={{marginLeft:10, marginTop:-100, fontSize:18, color:'#6787DB'}}>Dr. Rakesh Chavan</p>
                        <p style={{marginTop:-150, marginLeft:180}}>2 Sept</p>
                    </Box>
                    
                </div>
                <div style={{marginTop:30, marginLeft:20}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:870, height:325}}>
                        <HealthCondition/>
                    </Box>
                    
                </div>
                <div style={{marginLeft:905, marginTop:-325}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:530, height:200}}>
                        <p style={{marginLeft:10 , fontSize:16, marginTop:5}}>Lab Results</p>
                        <Grid container spacing={3}>
                            <Grid item xs>
                                <p style={{fontSize:12, marginLeft:15, marginTop:-170}}>5 August</p>
                                <p style={{fontSize:15, marginLeft:15, color:'brown', marginTop:20}}>Sugar</p>
                                <p style={{marginTop:20, marginLeft:15}}>140 mg/dL</p>
                            </Grid>
                            <Grid item xs>
                                <p style={{fontSize:12, marginLeft:15, marginTop:-170}}>5 August</p>
                                <p style={{fontSize:15, marginLeft:15, color:'brown', marginTop:20}}>Hemoglobin</p>
                                <p style={{marginTop:20, marginLeft:15}}>140 mg/dL</p>
                            </Grid>
                            <Grid item xs>
                                <p style={{fontSize:12, marginLeft:15, marginTop:-170}}>5 August</p>
                                <p style={{fontSize:15, marginLeft:15, color:'brown', marginTop:20}}>White Blood Cell</p>
                                <p style={{marginTop:20, marginLeft:15}}>140 mg/dL</p>
                            </Grid>
                        </Grid>
                        <Grid style={{marginTop:20}} container spacing={3}>
                            <Grid item xs={4}>
                                <p style={{fontSize:12, marginLeft:15, marginTop:-120}}>5 August</p>
                                <p style={{fontSize:15, marginLeft:15, color:'brown', marginTop:20}}>Lymohocyte</p>
                                <p style={{marginTop:20, marginLeft:15}}>3800</p>
                            </Grid>
                            <Grid item xs={4}>
                                <p style={{fontSize:12, marginLeft:15, marginTop:-120}}>5 August</p>
                                <p style={{fontSize:15, marginLeft:15, color:'brown', marginTop:20}}>Red Blood Cell</p>
                                <p style={{marginTop:20, marginLeft:15}}>4.9 million cells/mcL</p>
                            </Grid>
                            
                        </Grid>
                    </Box>
                </div>
                <div style={{marginLeft:905, marginTop:10}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:115, height:113}}>
                    <div style={{width:90, height:90, marginTop:10, marginLeft:10}}>
                    <CircularProgressbar value={Calories} text={`${Calories}Kcal`} />
                    </div>
                    
                    </Box>
                </div>
                <div style={{marginLeft:1035, marginTop:-112}}>
                    <Box style={{border:"2px solid rgba(0, 0, 0, 0.274)",borderRadius:10, width:400, height:113}}>
                        <SleepPattern />
                    </Box>
                </div>
            </Box>
        </div>
        </div>    
    )
}

export default Analytics
