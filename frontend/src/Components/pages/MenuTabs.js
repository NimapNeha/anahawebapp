import React, { useState } from 'react'
import '../../App.css'

import Analytics from './Analytics';
import Compliance from './Compliance';
function MenuTabs() {
    const [toggleState, setToggleState] = useState(1);

    const toggleTab = (index) => {
      setToggleState(index);
    };
    return (
        <div className="container" style={{marginLeft:30}}>
        <div className="bloc-tabs">
          <button
            style={{marginLeft:10}}
            className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(1)}
          >
            <span style={{fontSize:16}}>
            Analytics
                </span>  
            
          </button>
          <button
            className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(2)}
          >
            <span style={{fontSize:16}}>
                Compliance
                </span>
          </button>
         
        </div>
  
        <div className="content-tabs">
          
          <div
            className={toggleState === 1 ? "content  active-content" : "content"}
          > 
          <Analytics/>
        </div>
  
          <div
            className={toggleState === 2 ? "content  active-content" : "content"}
          >
            <Compliance/>
          </div>
  
          <div
            className={toggleState === 3 ? "content  active-content" : "content"}
          >
            <h2>Content 3</h2>
            <hr />
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos sed
              nostrum rerum laudantium totam unde adipisci incidunt modi alias!
              Accusamus in quia odit aspernatur provident et ad vel distinctio
              recusandae totam quidem repudiandae omnis veritatis nostrum
              laboriosam architecto optio rem, dignissimos voluptatum beatae
              aperiam voluptatem atque. Beatae rerum dolores sunt.
            </p>
          </div>
        </div>
      </div>
    )
}

export default MenuTabs
