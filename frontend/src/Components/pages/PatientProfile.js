import React, { useState } from 'react'
import MenuTabs from './MenuTabs'
import SearchIcon from '@material-ui/icons/Search'
import { Box, fade, InputBase, makeStyles } from '@material-ui/core'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
// const useStyles = makeStyles((theme)=>({
//     search: {
//         position: "relative",
//         borderRadius: theme.shape.borderRadius,
//         backgroundColor: fade(theme.palette.common.white, 0.15),
//         "&:hover": {
//           backgroundColor: fade(theme.palette.common.white, 0.25)
//         },
//         marginRight: theme.spacing(2),
//         marginLeft: 0,
//         width: "100%",
//         [theme.breakpoints.up("sm")]: {
//           marginLeft: theme.spacing(3),
//           width: "auto"
//         }
//       },
// }))

function PatientProfile() {
    // const classes = useStyles()
    return (
        <div>
            <div style={{marginTop:10}}>
            <span style={{fontSize:28, fontWeight:'bolder', marginLeft:40}}>ANAHA</span>
            </div>
            
            <p style={{marginTop:-30,fontSize:18, marginLeft:200}}>Patient Profile</p>
            {/* <div style={{marginRight:550, marginTop:-35}}>
                <Box bgcolor='white' style={{marginLeft:550,border:"1px solid rgba(0, 0, 0, 0.274)", height:50}}>
                    
                    <SearchIcon style={{marginTop:8, marginLeft:10, fontSize:35, fontWeight:'normal'}} />
                    <div style={{marginTop:-40}}>
                    <InputBase  style={{marginLeft:50, fontSize:18, color:'black'}} placeholder="Search">
                    </InputBase>
                    
                    </div>
                    <div style={{marginTop:-33}}>
                    <KeyboardArrowDownIcon style={{ marginLeft:385, fontSize:35}}/>
                    </div>
                    
                </Box>
             
            </div> */}
            <MenuTabs/>
        </div>
    )
}

export default PatientProfile
