import React from 'react'
import ReactApexChart from 'react-apexcharts';

function HeartRate() {
    const options={
        chart:{
            id: "basic-line",
            type:'area',
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                  enabled: true,
                  delay: 150
              },
              dynamicAnimation: {
                  enabled: true,
                  speed: 350
              },
            }
          },
    
          stroke:{
            curve:'smooth'
          },
          markers: {
            size: 1,
        },
          xaxis:{
            categories: ["9AM", "10AM", "11AM", "12PM", "1PM", "2PM"]
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value + "bpm";
              }
            },
          },
          title:{
            text:'Heart Rate',
            style:{
              fontWeight:'normal',
              fontSize:10,
            
            }
          }
          
        },
    
    series=[
          {
            name: "Health Rate",
            data: [100,70,110,120,90,140]        
            }
        ]
    return (
        <div>
             <ReactApexChart
                width="280"
                height="130"
                options={options}
                series={series}
                type='line'   
                />
        </div>
    )
}

export default HeartRate
