import React from 'react'
import ReactApexChart from 'react-apexcharts';
import Chart from 'react-apexcharts'
function HealthCondition() {
    const options={
        chart:{
            id: "basic-line",
            type:'area',
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                  enabled: true,
                  delay: 150
              },
              dynamicAnimation: {
                  enabled: true,
                  speed: 350
              },
            }
          },
          fill: {
            type: "gradient",
            gradient: {
              shadeIntensity: 1,
              opacityFrom: 0.7,
              opacityTo: 0.9,
              stops: [0, 90, 100]
            }
          },
          stroke:{
            curve:'smooth'
          },
          markers: {
            size: 0,
        },
          xaxis:{
            categories: ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sup","Oct", "Nov", "Dec"]
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value + "%";
              }
            },
          },
          title:{
            text:'Health Conditions',
            style:{
              fontWeight:'normal',
              fontSize:15,
            
            }
          }
          
        },
    
    series=[
          {
            name: "Health Condition",
            data: [67, 40, 50, 60, 49, 60, 70, 91, 50, 68, 45, 100]
          }
        ]
    
    return (
        <div>
        
            <div>
                <Chart
                options={options}
                series={series}
                type='area'
                width="860"
                height="310"
                
                />
            </div>
        </div>
    )
}

export default HealthCondition
