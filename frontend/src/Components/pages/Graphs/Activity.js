import React from 'react'
import ReactApexChart from 'react-apexcharts';

function Activity() {
    const options={
        chart:{

            
            // animations: {
            //   enabled: true,
            //   easing: 'easeinout',
            //   speed: 800,
            //   animateGradually: {
            //       enabled: true,
            //       delay: 150
            //   },
            //   dynamicAnimation: {
            //       enabled: true,
            //       speed: 350
            //   },
            // }
          },
          
         
          
          xaxis:{
            categories: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ]
          },
         
          title:{
            text:'Activity',
            style:{
              fontWeight:'normal',
              fontSize:10,
            
            }
          }
          
        },
    
    series=[
          {
            name: "Activity",
            // data: [35, 21, 15, 30, 8, 29, 20]
            data: [
                [1, [42, 6, 3, 9]],
                [2, [30, 6,42, 50]],
                [3, [20, 60,20, 60]],
                [4, [40, 60,20, 60]],
                [5, [10, 60,20, 60]],
                [6, [10, 60,20, 60]],
                [7, [50, 6,50, 6]], 
              ]
          }
        ]
    return (
        <div>
             <ReactApexChart
                options={options}
                series={series}
                type='candlestick'
                width="280"
                height="130"
                
                />
            
        </div>
    )
}

export default Activity
