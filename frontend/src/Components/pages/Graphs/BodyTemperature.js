import React from 'react'
import ReactApexChart from 'react-apexcharts'

function BodyTemperature() {
    const options={
        chart:{
            id: "basic-line",
            type:'area',
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                  enabled: true,
                  delay: 150
              },
              dynamicAnimation: {
                  enabled: true,
                  speed: 350
              },
            }
          },
    
          stroke:{
            curve:'smooth'
          },
          markers: {
            size: 1,
        },
          xaxis:{
            categories: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
          },
         
          title:{
            text:'Body Temperature',
            style:{
              fontWeight:'normal',
              fontSize:10,
            
            }
          }
          
        },
    
    series=[
          {
            name: "Body Temperature",
            data: [20, 30, 50, 45, 37, 52, 28]        
            }
        ]
    return (
        <div>
             <ReactApexChart
                width="270"
                height="130"
                options={options}
                series={series}
                type='area'   
                />
        </div>
    )
}

export default BodyTemperature
