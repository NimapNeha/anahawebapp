import React from 'react'
import ReactApexChart from 'react-apexcharts';

function SleepPattern() {
    const options={
        chart:{
            id: "basic-line",
            type:'area',
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                  enabled: true,
                  delay: 150
              },
              dynamicAnimation: {
                  enabled: true,
                  speed: 350
              },
            }
          },
          fill: {
            type: "gradient",
            gradient: {
              shadeIntensity: 1,
              opacityFrom: 0.7,
              opacityTo: 0.9,
              stops: [0, 90, 100]
            }
          },
          stroke:{
            curve:'smooth'
          },
          markers: {
            size: 0,
        },
          xaxis:{
            categories: ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
          },
          
          title:{
            text:'Sleep Pattern',
            style:{
              fontWeight:'normal',
              fontSize:10,
            
            }
          }
          
        },
    
    series=[
          {
            name: "Sleep pattern",
            data: [15, 12, 10, 8, 13,20]
          }
        ]
    return (
        <div>
             <ReactApexChart
                options={options}
                series={series}
                type='area'
                width="400"
                height="120"
                
                />
        </div>
    )
}

export default SleepPattern
